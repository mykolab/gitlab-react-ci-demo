#!/bin/bash

# Build images for staging, production, etc and push it to the registry.

func_inc=0
info() {
  func_inc=$((func_inc+1));
  local color='\033[0;32m'
  local nc='\033[0m';
  echo -e "\n${color} $func_inc. $1${nc}";
}

error() {
  local color='\033[0;31m'
  local nc='\033[0m';
  echo -e "\n${color}Error: $1${nc}";
}

set -e

tag_name="$1"
image_name="frontend-app"
node_version=14
image_registry="registry.gitlab.com/mykolab/gitlab-react-ci-demo/$image_name:$tag_name"

if [ -z "$tag_name" ]; then
    error "The tag name was not passed as an argument." >&2
    exit 1
fi

info "Node version is $node_version"
info "Building '$image_name' image..."

docker build --build-arg NODE_VERSION="$node_version" -f ./Dockerfile -t $image_registry ..

info "Image '$image_name' was successfully built."
info "Pushing '$image_name' image to '$image_registry' registry..."

docker push $image_registry

info "Image '$image_name' was successfully pushed to '$image_registry' registry."
