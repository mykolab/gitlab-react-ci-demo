stages:
  - setup
  - test
  - build
  - deploy

variables:
  NODE_VERSION: 14
  PROJECT_DOCKER_DIR: /var/www/frontend
  PROJECT_USER: web

##############
# Conditions #
##############
.if-staging-merge-request: &if-staging-merge-request
  if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "dev"'

.if-production-merge-request: &if-production-merge-request
  if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'

.if-staging-branch: &if-staging-branch
  if: '$CI_COMMIT_BRANCH == "dev"'

.if-production-branch: &if-production-branch
  if: '$CI_COMMIT_BRANCH == "main"'

.rules:default:
  rules:
    - <<: *if-staging-merge-request
    - <<: *if-production-merge-request
    - <<: *if-staging-branch
    - <<: *if-production-branch

##############
# Jobs #
##############
Install dependencies:
  extends:
    - .rules:default
  image: node:${NODE_VERSION}-alpine
  stage: setup
  artifacts:
    paths:
      - node_modules
    expire_in: 1 hour
  script:
    - npm install

Run eslint:
  extends:
    - .rules:default
  stage: test
  image: node:${NODE_VERSION}-alpine
  script:
    - npm run lint
  needs:
    - Install dependencies

Run tests:
  extends:
    - .rules:default
  stage: test
  image: node:${NODE_VERSION}-alpine
  script:
    - npm run test
  needs:
    - Install dependencies

Build application:
  rules:
    - <<: *if-staging-branch
    - <<: *if-production-branch
  stage: build
  image: node:${NODE_VERSION}-alpine
  artifacts:
    paths:
      - build
    expire_in: 1 week
  script:
    - npm run build
  needs:
    - Install dependencies
    - Run eslint
    - Run tests

.base-deployment: &base-deployment
  stage: deploy
  needs:
    - Build application
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - RELEASE_FOLDER_NAME=$(date '+%Y%m%d-%H%M%S')
    - echo "Start deploy to staging ..."
    - ssh-add <(echo "$DEPLOYMENT_SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - scp -r build $PROJECT_USER@$HOST_IP:$PROJECT_DOCKER_DIR/$RELEASE_FOLDER_NAME
    - ssh -t $PROJECT_USER@$HOST_IP "cd $PROJECT_DOCKER_DIR && ln -nfs $PROJECT_DOCKER_DIR/$RELEASE_FOLDER_NAME $PROJECT_DOCKER_DIR/current"

Deploy staging:
  <<: *base-deployment
  rules:
    - <<: *if-staging-branch
  environment:
    name: staging
    url: https://front.db-demo.bokoch-dev.site

Deploy production:
  <<: *base-deployment
  rules:
    - <<: *if-production-branch
  environment:
    name: production
    url: https://front.production.db-demo.bokoch-dev.site